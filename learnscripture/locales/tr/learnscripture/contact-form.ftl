## Contact form page:

# Page title
contact-form-page-title = Bizimle irtibata geçin

# Caption for the user's name input box
contact-form-name = Adınız

# Caption for the user's email address input box
contact-form-email = e-Posta adresiniz

# Help text for the 'message' when using the contact form
contact-form-message = Mesaj
                     .help-text = Kullanımda bir sorun veya hata bildirmek istersiniz, durumu çok net bir şekilde tanımlayını. Kullandığınız cihaz ve tarayıcıyı ve sürümlerini eklemeye unutmayınız.

# Button to send the message.
contact-form-send-button = Gönder

# Displayed above the contact form
contact-form-page-top-html =
    <p>Bizimle irtibata geçmek için aşağıdaki formu kullanabilirsiniz:</p>

    <p>İrtibat kurmadan önce, sorunuzun aşağıdaki sayfaların birisinde cevaplanmadığını kontrol ediniz:</p>

    <ul>
      <li><a href="/faq/">Sıkça Sorulan Sorular</a></li>
      <li><a href="/help/">Yardım</a></li>
      <li><a href="/bible-versions/">Kutsal Kitap Çevirileri</a></li>
      <li><a href="/handhelds/">Mobil cihazlar</a></li>
      <li><a href="/browsers/">Web tarayıcıları</a></li>
    </ul>

    <p>Yukarıda cevaplanan soruları içeren iletilere cevap veremeyeceğız.</p>

    <p>Sonraki adımınız <a href="/groups/website-help/">Site Yardım Grubu</a>nda bulunan diğer kullanıcılarından yardım talep etmektir.</p>

    <p>Sorunuz bunlar dışında olursa, size cevap yazmaya çabalarız. Ancak,
    <b>LearnScripture.net sitesine gönüllüler bakmaktadır</b>.
    İletinize hızlı bir şekilde cevap gelmezse, kusurumuza bakmayın.</p>


# Displayed above the contact form
contact-form-page-bottom-html =
  <p>Bize ayrıca şöylece ulaşabilirsiniz:</p>
  <ul>
    <li>e-Posta: <a href="mailto:contact@learnscripture.net">contact@learnscripture.net</a></li>
    <li>Twitter: <a class="twitter-mention-button" data-related="learnscripture" href="https://twitter.com/intent/tweet?screen_name=learnscripture">@learnscripture</a></li>
  </ul>

contact-thanks-page-title = Teşekkürler

contact-thanks-for-your-message = Mesajınız için teşekkür ederiz!
