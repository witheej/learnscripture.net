## Contact form page:

# Page title
contact-form-page-title = Contact us



# Caption for the user's name input box
contact-form-name = Name

# Caption for the user's email address input box
contact-form-email = Email

# Help text for the 'message' when using the contact form
contact-form-message = Message
                     .help-text = If you are reporting a problem, please include a full and specific description, and include what device/browser you are using, with version numbers.

# Button to send the message.
contact-form-send-button = Send

# Displayed above the contact form
contact-form-page-top-html =
    <p>You can use the form below to contact us:</p>

    <p>Before contacting us, please check whether your question is answered on any of these pages:</p>

    <ul>
      <li><a href="/faq/">Frequently Asked Questions</a></li>
      <li><a href="/help/">Help</a></li>
      <li><a href="/bible-versions/">Bible versions</a></li>
      <li><a href="/handhelds/">Handheld devices (phones and tablets)</a></li>
      <li><a href="/browsers/">Web browsers</a></li>
    </ul>

    <p>We’re sorry that we won’t be able to answer queries that are already answered above.</p>

    <p>The next place to ask questions is the <a href="/groups/website-help/">Website Help Group</a>, where you can get help from other users.</p>

    <p>For other queries, if it is something urgent we will try to respond. However,
    <b>the LearnScripture.net site is volunteer run</b>.
    Please don't be offended if your query does not receive a fast reply!</p>


# Displayed above the contact form
contact-form-page-bottom-html =
  <p>Alternatively you can:</p>
  <ul>
    <li>Email us: <a href="mailto:contact@learnscripture.net">contact@learnscripture.net</a></li>
    <li>Tweet us: <a class="twitter-mention-button" data-related="learnscripture" href="https://twitter.com/intent/tweet?screen_name=learnscripture">Tweet to @learnscripture</a></li>
  </ul>


contact-thanks-page-title = Thanks!

contact-thanks-for-your-message = Thanks for your message, we'll deal with it as soon as we can.
